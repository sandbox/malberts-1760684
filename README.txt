Commerce PayPal WPP 3DS
=======================

Install this module as any other module.

It does not require any configuration. If your PayPal WPP payment instance is
configured to use the 3-D Secure API, then your outgoing requests will be
altered to include the relevant 3-D Secure data.
